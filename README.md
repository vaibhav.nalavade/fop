

# Katalon Sample Project

This is a sample katalon project which can be used for Mobile,Web and API automation. This project tries to define best practices and coding standards that should be followed while developing automation scripts using Katalon platform.

# Coding Standards and Best Practices
### 1) Naming Conventions 
  #### Test Cases 
   - Test Case name should be Capitalized Case (Capitalize first letter of each word). Test Case name should clearly indicate its purpose. 
  - Valid Examples :
    Login And verify, Order And checkout Using Coupon
 - Invalid Examples :
    login_and_verify, VerifyAndLogin,verify_and login
#### Objects and Pages
   - Pages name should be Capitalized Case. Page name should have Page suffix.
   - Valid Examples :
     Login Page, Home Page
   - Test Object name should adhere to following naming convention : 
   < elementType >_< description >
   Valid Test Objects Names 
    input_User name, a_Forgot your password
#### Test Suites and Test Data Files
- Test Suites and Test data files name should follow Capitalized Case convention.
- Test varibles and Test Data file column name should be kept same for easier identification by Katalon.

#### Groovy Files and Custom Keywords
- All Groovy files (custom keywords, listeners, scripts etc) should follow camel case naming convention. 
- Grrovy package should follow reverse domain naming convention. Example : com.katalon.util

#### Variables
- Variables name should follow camelCase naming convention. 
- Valid Examples : userName, companyName, isUserPresentInList
- Global variables should start with g_ prefix. Format : g_< variableName >
- Valid Examples for global variables : g_siteUrl,g_homePageUrl

### 2) Code Reusablity
- We can follow page object model in katalon for workflow reusability. 
- A business workflow can be abstracted as test case in Page Objects folder and can be reused in another test cases using "call test case" keyword provided.

### 3) Grouping of Test cases, test suites
- Relevant test cases should be grouped for clarity. 
- All test cases related to a module should be grouped in a folder named for module. SubModule folders should be created within module folder if required. 
- Test suites can be grouped using test suite collection.

### 4) Test Data Management
- Excel/CSV should be used to store test data. Use Katalon's Test Data folder to import test data files. 
- Relevant Test Data object should be grouped using folders. 
- Create excel file for each module containing sheets for each submodule.

### 5) Commenting Test Cases
- Use comment keyword to add description for each test case clearly indicating its purpose.

### 7) Keep it Clean 
- Please do not create unnecessary test cases, test suites, properties file, variables, profiles etc. 
- Keep the code clean as much as possible.

### 9) Custom Keywords
- Custom keyword should be used to created only if there is no action keyword present in Katalon. 
- Please do not use custom keywords to write test workflow. Example : for login to a web page you can create custom keyword which would login but it is not recommended, create a test case instead and call that test case wherever required. 
- Please follow groovy best practices to write groovy scripts (https://groovy-lang.org/style-guide.html)
- Please follow below link to understand how to create custom keywords : 
(https://docs.katalon.com/katalon-studio/docs/introduction-to-custom-keywords.html)

### 10) Creating End To End test Cases
- End to end test cases can created by combining various test cases of web,mobile and api. 
- Use "Call Test Case" keyword to call relevant test cases to created e2e test case.

### 11) Maintaining Test Environment using Profiles
- Use Katalon profiles to maintain attributes/variables regardin different testing environment(QA,DEV,PROD etc).

### 12) Git Branching Strategy
- Create a branch named "develop" from master to maintain code. Merge Code with Master branch after test cases a user story is completed.  
- After development create a pull request to merge develop branch with master branch.
- Hotfix branch from master branch can be created to fix urgent bugs or production issues. Naming conventions for hotfix branch : hotfix_< description >
- Hotfix branch should be merged with both master and develop branch.
- You can use git plugin in katalon to pull and push code.

### 13) Command line execution and DevOps Integration
- Test Suites can be executed from command line. Katalon command line tool should be used for CI/CD integration. Pleas follow below link for more information : 
(https://docs.katalon.com/katalon-studio/docs/console-mode-execution.html)

# Additional Links
- [Katalon Docs](https://docs.katalon.com/katalon-studio/docs/index.html)
- [Katalon Command Line](https://docs.katalon.com/katalon-studio/docs/console-mode-execution.html)
- [Groovy Style Guide](https://groovy-lang.org/style-guide.html)
